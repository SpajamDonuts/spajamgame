﻿using UnityEngine;
using System.Collections;

public class FadeInfo : MonoBehaviour {

	/// <summary>フェード色</summary>
	[SerializeField]
	private Color fadeColor;
	/// <summary>フェードアウト時間（秒）</summary>
	[SerializeField]
	private float fadeOutTime_secs = 0;
	/// <summary>フェード間の待機時間（秒）</summary>
	[SerializeField]
	private float waitTime_secs = 0;
	/// <summary>フェードイン時間（秒）</summary>
	[SerializeField]
	private float fadeInTime_secs = 0;
	/// <summary>シーン変更をするかどうか</summary>
	[SerializeField]
	private bool usesChangeScene = false;
	/// <summary>シーン変更をする場合、次のシーンの名前</summary>
	[SerializeField]
	private string nextSceneName = "";

	public Color FadeColor {
		get { return fadeColor; }
		set { fadeColor = value; }
	}

	public float FadeOutTime_secs {
		get { return fadeOutTime_secs; }
	}

	public float WaitTime_secs {
		get { return waitTime_secs; }
	}

	public float FadeInTime_secs {
		get { return fadeInTime_secs; }
	}

	public bool UsesChangeScene {
		get { return usesChangeScene; }
	}

	public string NextSceneName {
		get { return nextSceneName; }
	}

}
